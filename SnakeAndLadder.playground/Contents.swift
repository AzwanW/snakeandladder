//: Playground - noun: a place where people can play

import UIKit

var str = "Hello, playground"


var playerPosition: Int = 0
//var player2Position: Int = 0
var round: Int = 0

var board: [Int : Int] = [:]

board [10] = 25 //ladder
board [20] = -15 //snake
board [40] = 60 //ladder
board [80] = -22 //snake
board [11] = 26 //ladder
board [21] = -14 //snake
board [41] = 71 //ladder
board [81] = -21 //snake


while (playerPosition < 100) {
    
    //Round
    round = round + 1
    print (" ")
    print (">> Round number \(round)")
    
    
    //Roll dice
    var dice: Int = Int(arc4random_uniform (6)) + 1
    
    //Player position
    playerPosition = playerPosition + dice
    print ("Dice number is \(dice) Player is at position \(playerPosition)")
    
    if let snakeOrLadders = board[playerPosition] {
        playerPosition = playerPosition + snakeOrLadders
        
        if (snakeOrLadders < 0){
            print("Player landed on a snake! Player move down \(snakeOrLadders) steps")
        }
        else {
            print("Player landed on a ladder! Player move up \(snakeOrLadders) steps")
        }
        print (" ")
        print ("Player is at position \(playerPosition)")
    }
    
}










/*var colors: [String] = ["White", "Blue", "Red"]
    for (var i = 0; i < colors.count; i++){
print(colors[i])

}
*/
